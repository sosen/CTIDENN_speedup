#!/bin/bash
#
mkdir -p rundir_vt;
cd rundir_vt;
cp ../jobOptions_StandaloneDBM_RDO.py .;
#
lsetup "asetup Athena,21.0,latest"
source /cvmfs/projects.cern.ch/intelsw/psxe/linux/all-setup.sh
#
amplxe-cl -collect hotspots -strategy=:trace:trace,ld-linux.so.2:notrace:notrace,ld-2.12.so:notrace:notrace,ld-linux.so:notrace:notrace,ld-linux-x86-64.so.2:notrace:notrace -- \
athena jobOptions_StandaloneDBM_RDO.py > __reco.txt 2>&1;
#
cd ..;
