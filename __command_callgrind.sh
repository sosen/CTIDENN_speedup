#!/bin/bash
#
mkdir -p rundir_cg;
cd rundir_cg;
#
lsetup "asetup Athena,21.0,latest"
#
athena jobOptions_StandaloneDBM_RDO.py \
  --ignoreErrors='True' \
  --valgrind='True' \
  --valgrindDefaultOpts='False' \
  --valgrindExtraOpts='--tool=callgrind,--trace-children=yes,--num-callers=8,--collect-jumps=yes,--instr-atstart=no' \
  --athenaopts='--stdcmalloc' > __reco.txt 2>&1;
#
cd ..;
