#!/bin/bash
#
mkdir -p rundir_cg;
cd rundir_cg;
cp ../jobOptions_StandaloneDBM_RDO.py .;
#
lsetup "asetup Athena,21.0,latest"
#
valgrind --tool=callgrind --trace-children=yes `which athena.py` --stdcmalloc jobOptions_StandaloneDBM_RDO.py >& log;
#
cd ..;
